package proto_parser

import (
	"github.com/emicklei/proto"
	"regexp"
	"strings"
)

func parseSrvGenRPC(srv *proto.Service) {
	if srv.Comment == nil {
		return
	}

	if len(srv.Comment.Lines) == 0 {
		return
	}

	var needGen bool
	var svcDesc, genTo string
	var doc = srv.Comment.Lines

	for _, com := range doc {
		// 是否需要生成service代码
		if strings.Contains(com, "@rpc_gen") {
			var gr = regexp.MustCompile(RegexpRpcGen)
			res := gr.MatchString(com)
			if res {
				needGen = true
				continue
			}
		}

		// service 注释
		if strings.Contains(com, "@desc") {
			reg := regexp.MustCompile(RegexpRouterRpcDesc)
			res := reg.FindAllStringSubmatch(com, -1)
			if len(res) == 1 && len(res[0]) == 2 {
				svcDesc = trim(res[0][1])
				continue
			}
		}

		// service 生成位置
		if strings.Contains(com, "@gen_to") {
			var gt = regexp.MustCompile(RegexpRouterGenTo)
			res := gt.FindAllStringSubmatch(com, -1)
			if len(res) == 1 && len(res[0]) == 2 {
				genTo = trim(res[0][1])
				continue
			}
		}
	}

	if !needGen {
		return
	}

	// 开始操作一拨
	genServiceAllRpc(srv, svcDesc, genTo)
}

func genServiceAllRpc(srv *proto.Service, svcDesc, genTo string) {
	// 取 genTo 不存在就创建文件
	// 检测是否实现接口 没实现则生成实现方法
}
