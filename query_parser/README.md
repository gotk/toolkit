schema
======
> 对 github.com/gorilla/schema 包的扩展, schema 用来将url.Values结构映射到 structs 上.

对于默认的 NewDecoder/NewEncoder, 你必须指定tag "schema". 

```go
type Person struct {
    Name  string `schema:"name,required"`  // custom name, must be supplied
    Phone string `schema:"phone"`          // custom name
    Admin bool   `schema:"-"`              // this field is never set
}
```

对于扩展的 NewDecoderCustom, 你需要手动指定tag.

> 一个示例 NewDecoderCustom("json")

```go
type Person struct {
    Name  string `json:"name"`           // custom name
    Phone string `json:"phone"`          // custom name
    Admin bool   `json:"-"`              // this field is never set
}
```