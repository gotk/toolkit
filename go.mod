module gitlab.com/gotk/toolkit

go 1.16

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/elliotchance/pie v1.39.0
	github.com/emicklei/proto v1.9.2
	github.com/emicklei/proto-contrib v0.9.2
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/jhump/protoreflect v1.12.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220222200937-f2425489ef4c // indirect
	google.golang.org/genproto v0.0.0-20220222213610-43724f9ea8cf // indirect
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
